package com.spring.taskmanager.data;

import java.util.Date;

public class TaskPayload {
    private String title;
    private String description;
    private Date targetDate;
    private boolean done;

    public TaskPayload(String title, String description, Date targetDate, boolean isDone) {
        this.title = title;
        this.description = description;
        this.targetDate = targetDate;
        this.done = isDone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        done = done;
    }
}
