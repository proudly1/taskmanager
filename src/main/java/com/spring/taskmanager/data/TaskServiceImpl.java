package com.spring.taskmanager.data;

import com.spring.taskmanager.data.repositories.TasksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    private TasksRepository tasksRepository;

    @Autowired
    public TaskServiceImpl(TasksRepository tasksRepository) {
         this.tasksRepository = tasksRepository;
    }

    @Override
    public Task save(Task task) {
        if (task != null) {
            return tasksRepository.save(task);
        }
        return null;
    }

    @Override
    public void delete(Task task) {
        if (task != null) {
            tasksRepository.delete(task);
        }
    }

    @Override
    public Iterable<Task> findAll() {
        return tasksRepository.findAll();
    }

    @Override
    public Task findById(Long id) {
        if (id != null) {
            Optional<Task> task = tasksRepository.findById(id);

            if (task.isPresent())
                return  task.get();
        }
        return  null;
    }
}
