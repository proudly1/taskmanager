package com.spring.taskmanager.data;

import java.util.List;

public interface TaskService {

    Task save(Task task);

    void delete(Task task);

    Iterable<Task> findAll();

    Task findById(Long id);
}
