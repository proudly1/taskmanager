package com.spring.taskmanager.data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "Title is required!")
    @Column(name = "title", length = 200)
    private String title;

    @NotBlank(message = "Description is required!")
    private String description;

    @Column(name = "createAt")
    private Date createdAt;
    @Column(name = "target_date")
    private Date targetDate;
    @Column(name = "is_done")
    private boolean isDone;




    @PrePersist
    private void createdAt() {
        this.createdAt = new Date();
    }


    public Task() {
    }

    public Task(String title, String description, Date targetDate, boolean isDone) {
        this.title = title;
        this.description = description;
        this.targetDate = targetDate;
        this.isDone = isDone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}
