package com.spring.taskmanager.web.controllers;

import com.spring.taskmanager.data.Task;
import com.spring.taskmanager.data.TaskPayload;
import com.spring.taskmanager.data.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public ResponseEntity<Iterable<Task>> getTasks(Model model) {
        return ResponseEntity.ok(taskService.findAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<Task> getTask(@PathVariable long id) {
        Task task = taskService.findById(id);

        if (task == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(task);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> createTask(@RequestBody TaskPayload payload) {

        Task task = taskService.save(new Task(payload.getTitle(), payload.getDescription(), payload.getTargetDate(), payload.isDone()));

        return ResponseEntity.ok(task);
    }

    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> modifyTask(@PathVariable long id, @RequestBody TaskPayload payload) {
        Task task = taskService.findById(id);

        if (task == null)
            return ResponseEntity.notFound().build();

        task.setTitle(payload.getTitle());
        task.setDescription(payload.getDescription());
        task.setDone(payload.isDone());
        task.setTargetDate(payload.getTargetDate());
        taskService.save(task);

        return ResponseEntity.ok(task);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Task> deleteTask(@PathVariable long id) {
        Task task = taskService.findById(id);

        if (task == null)
            return ResponseEntity.notFound().build();

        taskService.delete(task);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/csrf-token")
    public ResponseEntity<String> getToken(HttpServletRequest request) {
        CsrfToken token = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
        return ResponseEntity.ok(token.getToken());
    }
}
