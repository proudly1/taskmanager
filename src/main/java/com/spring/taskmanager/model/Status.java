package com.spring.taskmanager.model;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
